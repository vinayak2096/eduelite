/** 
* @api {post} rest/Exams/feedbacks.json Submit Feedback
* @apiName Submit Feedback
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body ) {Number} exam_result_id  User exam's result id
*@apiParam (Body ) {String} private_key   private_key
*@apiParam (Body ) {String} public_key  public_key
*@apiParam (Body ) {String} responses.comment1  comments

*@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   