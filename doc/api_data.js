define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "E__EduEliteAPI_doc_main_js",
    "groupTitle": "E__EduEliteAPI_doc_main_js",
    "name": ""
  },
  {
    "type": "post",
    "url": "rest/Registers/save.json",
    "title": "User Registration",
    "name": "Registration",
    "group": "EduElite",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Users password.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Users name.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Users phone.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "guardian_phone",
            "description": "<p>Users guardian_phone.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "enroll",
            "description": "<p>Users enroll.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "group_name",
            "description": "<p>Users group_name.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./UserRegistration.js",
    "groupTitle": "EduElite"
  },
  {
    "type": "post",
    "url": "rest/Checkouts/coupon.json",
    "title": "Check Coupon",
    "name": "Check_Coupon",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "total_amount",
            "description": "<p>coupon_code</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>coupon_code</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./CheckCoupon.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Dashboards/remainingExam.json",
    "title": "Check Exam",
    "name": "Check_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./CheckExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Checkouts/coupon.json",
    "title": "Check Coupon",
    "name": "Delete_Coupon",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "total_amount",
            "description": "<p>coupon_code</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>coupon_code</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./DeleteCoupon.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Profiles/editProfile.json",
    "title": "Edit Profile",
    "name": "Edit_Profile",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "Number",
            "optional": false,
            "field": "enroll",
            "description": "<p>enroll</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>phone</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "guardian_phone",
            "description": "<p>guardian_phone</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./EditProfile.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/view.json",
    "title": "Exam Details",
    "name": "Exam_Details",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./ExamDetails.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "Get",
    "url": "rest/Exams/start.json",
    "title": "Get Exam",
    "name": "Get_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>users id</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/expired.json",
    "title": "Get Expired Exam",
    "name": "Get_Expired_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetExpiredExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/free.json",
    "title": "Get Free Exam",
    "name": "Get_Free_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetFreeExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Registers/group.json",
    "title": "Get Group Scalar",
    "name": "Get_Group_Scalar",
    "group": "User_Authentication",
    "version": "0.1.0",
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./getGroupScalar.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Helps.json",
    "title": "Get Help Data",
    "name": "Get_Help_Data",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetHelpData.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/instruction.json",
    "title": "Get Instructions",
    "name": "Get_Instructions",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetInstruction.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Leaderboards.json",
    "title": "Get Leader List",
    "name": "Get_Leader_List",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetLeaderList.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Dashboards/mail.json",
    "title": "Get Mail Count",
    "name": "Get_Mail_Count",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetMailCount.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Apis/emailcapturing.json",
    "title": "Get OTP",
    "name": "Get_OTP",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "capture_by",
            "description": "<p>capture_by</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetOTP.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "Checkouts/placeOrder",
    "title": "Get Place Order",
    "name": "Get_Place_Order",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "responses",
            "description": "<p>responses</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "couponCode",
            "description": "<p>couponCode</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>payment_gateway</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetPlaceOrder.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Apis/studentGroups.json",
    "title": "Get Profile Groups",
    "name": "Get_Profile_Groups",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetPlaceGroups.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/purchased.json",
    "title": "Get Purchased Exam",
    "name": "Get_Purchased_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetPurchasedExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Results/scorecard.json",
    "title": "Get Score Card",
    "name": "Get_Score_Card",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "result_id",
            "description": "<p>ResultID</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "student_id",
            "description": "<p>StudentID</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetScoreCard.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/StudyMaterial/index.json",
    "title": "Get Study Material",
    "name": "Get_Study_Material",
    "group": "User_Authentication",
    "version": "0.1.0",
    "filename": "./GetStudyMaterial.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Results/subjectreport.json",
    "title": "Get Subject Response",
    "name": "Get_Subject_Response",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "result_id",
            "description": "<p>ResultID</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "student_id",
            "description": "<p>StudentID</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetSubjectResponse.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/index.json",
    "title": "Get Today Exam",
    "name": "Get_Today_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetTodayExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Transactionhistorys/index.json",
    "title": "Get Transaction Data",
    "name": "Get_Transaction_Data",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetMyResult.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Transactionhistorys/index.json",
    "title": "Get Transaction Data",
    "name": "Get_Transaction_Data",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetTransactionData.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Exams/upcoming.json",
    "title": "Get Upcoming Exam",
    "name": "Get_Upcoming_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetUpcomingExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Profiles/index.json",
    "title": "Get User Profile",
    "name": "Get_User_Profile",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetUserProfile.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Dashboards/balance.json",
    "title": "Get Wallet Count",
    "name": "Get_Wallet_Count",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./GetWalletCount.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Users/login.json",
    "title": "Login",
    "name": "Login",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Users password.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./Login.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Forgots/password.json",
    "title": "Password Resend",
    "name": "Password_Resend",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./PasswordResend.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Checkouts/placeOrder.json",
    "title": "Place Older",
    "name": "Place_Older",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "Number",
            "optional": false,
            "field": "responses",
            "description": "<p>responses</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./PlaceOrder.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Emailverifications/resendEmail.json",
    "title": "Resend Verification Code",
    "name": "Resend_Verification_Code",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>users email.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./ResendVerificationCode.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Forgots/reset_password.json",
    "title": "Reset Password",
    "name": "Reset_Password",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "Verification_code",
            "description": "<p>Verification_code.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "Password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "Confirm_password",
            "description": "<p>Confirm_password.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./ResetPassword.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Exams/saveAll.json",
    "title": "Send Exam Response",
    "name": "Send_Exam_Response",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "student_id",
            "description": "<p>student_id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "exam_id",
            "description": "<p>exam_id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "start_time",
            "description": "<p>start_time</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "end_time",
            "description": "<p>end_time</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./sendResponseExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Exams/expiredFinish.json",
    "title": "Submit Expired Exam",
    "name": "Submit_Expired_Exam",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "exam_id",
            "description": "<p>exam_id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./SubmitExpiredExam.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Exams/feedbacks.json",
    "title": "Submit Feedback",
    "name": "Submit_Feedback",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "Number",
            "optional": false,
            "field": "exam_result_id",
            "description": "<p>User exam's result id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "responses.comment1",
            "description": "<p>comments</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./SubmitFeedback.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "rest/Apis/updateusertoken.json",
    "title": "Update User Token",
    "name": "Update_User_Token",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "usertoken",
            "description": "<p>usertoken</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./UpdateUserToken.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "Contacts/submit",
    "title": "Upload Data With Attachment",
    "name": "Upload_Data_With_Attachment",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./UploadDataWithAttachment.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "Contacts/submit",
    "title": "Upload Data Without Attachment",
    "name": "Upload_Data_Without_Attachment",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./UploadDataWithoutAttachment.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Profiles/changePhoto.json",
    "title": "UploadImage",
    "name": "Upload_Image",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "public_key",
            "description": "<p>public_key</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "private_key",
            "description": "<p>private_key</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./UploadImage.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "post",
    "url": "rest/Emailverifications/emailVerify.json",
    "title": "Verify OTP",
    "name": "Verify_OTP",
    "group": "User_Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "Verification_code",
            "description": "<p>Verification_code.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./VerifyOTP.js",
    "groupTitle": "User_Authentication"
  },
  {
    "type": "get",
    "url": "1.json",
    "title": "getExamScalar",
    "name": "getExamScalar",
    "group": "User_Authentication",
    "version": "0.1.0",
    "sampleRequest": [
      {
        "url": "url"
      }
    ],
    "filename": "./getExamScalar.js",
    "groupTitle": "User_Authentication"
  }
] });
