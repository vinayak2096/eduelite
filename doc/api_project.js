define({
  "name": "EduEliteAPIs",
  "version": "0.1.0",
  "description": "This document contains all the APIs used in eduElite",
  "title": "EduEliteAPIS",
  "url": "https://api.github.com/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-02-17T08:04:55.915Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
