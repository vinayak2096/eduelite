/**
* @api {post} rest/Registers/save.json User Registration
* @apiName Registration
* @apiGroup EduElite
 * @apiVersion 0.1.0
 * 

*@apiParam (Body) {String} email Users email.
*@apiParam (Body) {String} password Users password.
*@apiParam (Body) {String} name Users name.
*@apiParam (Body) {String} phone Users phone.
*@apiParam (Body) {String} guardian_phone Users guardian_phone.
*@apiParam (Body) {String} enroll Users enroll.
*@apiParam (Body) {String} group_name Users group_name.


@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   