/** 
* @api {get} Checkouts/placeOrder Get Place Order
* @apiName Get Place Order
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body ) {String} responses  responses
*@apiParam (Body ) {String} public_key  public_key
*@apiParam (Body ) {String} private_key  private_key 
*@apiParam (Body ) {String} couponCode  couponCode
*@apiParam (Body ) {String} payment_gateway  payment_gateway 


*@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   