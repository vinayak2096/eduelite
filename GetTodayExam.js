/**
* @api {get} rest/Exams/index.json Get Today Exam
* @apiName Get Today Exam
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body ) {String} public_key  public_key
*@apiParam (Body ) {String} private_key   private_key

@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   