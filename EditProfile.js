/** 
* @api {post} rest/Profiles/editProfile.json Edit Profile
* @apiName Edit Profile
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body ) {Number} enroll  enroll
*@apiParam (Body ) {String} private_key   private_key
*@apiParam (Body ) {String} public_key  public_key
*@apiParam (Body ) {String} phone  phone
*@apiParam (Body ) {String} guardian_phone  guardian_phone

*@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   