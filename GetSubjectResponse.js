/** 
* @api {get} rest/Results/subjectreport.json Get Subject Response
* @apiName Get Subject Response
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body ) {String} result_id ResultID
*@apiParam (Body ) {String} student_id StudentID   


*@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   