/**
* @api {get} rest/Dashboards/remainingExam.json Check Exam
* @apiName Check Exam
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body ) {String} public_key  public_key
*@apiParam (Body ) {String} private_key  private_key

@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   