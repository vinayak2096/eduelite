/**
* @api {post} rest/Forgots/password.json Password Resend
* @apiName Password Resend
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body) {String} email Users email.

@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   