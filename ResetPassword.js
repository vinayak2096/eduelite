/**
* @api {post} rest/Forgots/reset_password.json Reset Password
* @apiName Reset Password
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body) {String} Verification_code  Verification_code.
*@apiParam (Body) {String} Password  Password.
*@apiParam (Body) {String} Confirm_password  Confirm_password.
@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   