/**
* @api {post} rest/Emailverifications/resendEmail.json  Resend Verification Code
* @apiName  Resend Verification Code
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body) {String} email  users email.

@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   