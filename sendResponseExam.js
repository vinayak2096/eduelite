/**
* @api {post}   rest/Exams/saveAll.json  Send Exam Response
* @apiName  Send Exam Response
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 
*@apiParam (Body) {String} student_id  student_id
*@apiParam (Body) {String} exam_id  exam_id
*@apiParam (Body) {String} start_time   start_time
*@apiParam (Body) {String} end_time  end_time
*@apiParam (Body) {String} public_key  public_key
*@apiParam (Body) {String} private_key   private_key

*@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   