/**
* @api {Get}  rest/Exams/start.json  Get Exam
* @apiName  Get Exam
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body) {String} public_key  public_key
*@apiParam (Body) {String} private_key  private_key
*@apiParam (Body) {String} id  users id


@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   