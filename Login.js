/**
* @api {post} rest/Users/login.json Login
* @apiName Login
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 

*@apiParam (Body) {String} email Users email.
*@apiParam (Body) {String} password Users password.


@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   