/**
* @api {get}  1.json getExamScalar
* @apiName  getExamScalar
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 
*@apiSampleRequest url
*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   