/**
* @api {post} rest/Checkouts/coupon.json Check Coupon
* @apiName Check Coupon
* @apiGroup User Authentication
 * @apiVersion 0.1.0
 * 
 *@apiParam (Body ) {String} total_amount  coupon_code
*@apiParam (Body ) {String} coupon_code  coupon_code
*@apiParam (Body ) {String} public_key  public_key
*@apiParam (Body ) {String} private_key  private_key

@apiSampleRequest url

*/

function getUser() {
    return { code: 200, data: currentUser };
   }
   